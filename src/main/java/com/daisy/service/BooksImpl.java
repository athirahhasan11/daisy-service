package com.daisy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daisy.models.Books;
import com.daisy.repository.BookRepository;

@Service
public class BooksImpl  implements BookService{

	@Autowired
	BookRepository bookRepo;
	
	@Override
	public List<Books> findBookList() {
		return bookRepo.findAll();
	}

	
}
