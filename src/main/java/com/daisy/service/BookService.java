package com.daisy.service;

import java.util.List;

import com.daisy.models.Books;

public interface BookService {

	public List<Books> findBookList();
}
