package com.daisy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.daisy.models.Books;


public interface BookRepository extends JpaRepository<Books, Long> {

}
